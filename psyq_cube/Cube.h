/*
 * Cube.h
 */

#ifndef _CUBE_H_INCLUDED_
#define _CUBE_H_INCLUDED_

#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <stdio.h>

#include "Model.h"

#define NUM_CUBE_VERTICES 8
#define NUM_CUBE_FACES    12 // 6*2 -> front,back,top,bottom,left,right, 2 triangles each

/*
===============================================================================
Model

    Holds vertices and faces for a Cube, with all of the properties of Model
===============================================================================
*/
class Cube : public Model
{
public:
    /*
    ===================
    Constructor/Deconstructor
    ===================
    */
    Cube();
    ~Cube();

private:
    static SVECTOR m_DefaultVertices[NUM_CUBE_VERTICES];
    static SVECTOR m_DefaultNormals [NUM_CUBE_FACES];
    static CVECTOR m_DefaultColors  [NUM_CUBE_FACES];
    static INDEX   m_DefaultFaces   [NUM_CUBE_FACES];
};

#endif // _CUBE_H_INCLUDED_
