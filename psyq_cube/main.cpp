
#include <stdio.h>

#include "Light.h"
#include "System.h"
#include "Model.h"
#include "Pad.h"
#include "Cube.h"

// for(ever) ;)
#define ever ;;

int main(void)
{
    Cube cube;
    Cube cube2;
    Light light;
    SVECTOR *lightAngle;

    Pad pad1(0);

    // reset gpu, sys, etc.
    System::Init();

    cube.MoveTo(0, 0, 500);
    cube2.MoveTo(-200, 0, 400);

    for(ever)
    {
        pad1.Read();

        // rotate cube
        if ( pad1.IsHeld(PadUp) ) {
            cube.Rotate(5, 0, 0);
        } else if ( pad1.IsHeld(PadDown) ) {
            cube.Rotate(-5, 0, 0);
        }
        else if ( pad1.IsHeld(PadLeft) ) {
            cube.Rotate(0, -5, 0);
        }
        else if (pad1.IsHeld(PadRight)) {
            cube.Rotate(0, 5, 0);
        }

        // rotate light
        if ( pad1.IsHeld(PadTriangle) ) {
            light.Rotate(5, 0, 0);
        } else if ( pad1.IsHeld(PadCross) ) {
            light.Rotate(-5, 0, 0);
        }
        else if ( pad1.IsHeld(PadSquare) ) {
            light.Rotate(0, -5, 0);
        }
        else if (pad1.IsHeld(PadCircle)) {
            light.Rotate(0, 5, 0);
        }

        cube2.Rotate(8, 0, 15);

        cube.Draw(&light);
        cube2.Draw(&light);
        //cube.Draw((Light*)NULL);
        //cube.Draw(&lgtang, &rotlgt, &lgtmat, &light);
        //cube2.Draw(&lgtang, &rotlgt, &lgtmat, &light);
        //cube2.Draw((Light*)NULL);

        //System::DrawText(S_WIDTH>>1, S_HEIGHT>>1, "Hello World");
        lightAngle = light.GetAngle();
        FntPrint("Light Angle: %d, %d, %d", lightAngle->vx, lightAngle->vy, lightAngle->vz);

        System::Display();
    }

    return 0;
}


