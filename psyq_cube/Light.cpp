/*
 * Light.cpp
 */

#include "Light.h"

/*
===================
Constructor/Deconstructor
===================
*/
Light::Light()
{
    m_colorMat = (MATRIX){
// light source #0                #1  #2
                m_DefaultColor.vx, 0,  0, // R
                m_DefaultColor.vy, 0,  0, // G
                m_DefaultColor.vz, 0,  0  // B
    };

    m_angle = (SVECTOR){
        m_DefaultAngle.vx, m_DefaultAngle.vy, m_DefaultAngle.vz, 0
    };
    m_pos = (SVECTOR){
        m_DefaultPos.vx, m_DefaultPos.vy, m_DefaultPos.vz, 0
    };

    m_posMat = (MATRIX){
        m_pos.vx, m_pos.vy, m_pos.vz, // light source #0
        0,        0,        0,        //              #1
        0,        0,        0         //              #2
    };
}

/*
===================
Convert internal angle matrix to screen coords
===================
*/

// TODO - make this independant of the rottrans/give the light its own!
void Light::Translate( MATRIX *rottrans )
{
    MATRIX res; // resulting calculated matrix

    // calculate the angle matrix in relative screen coords
    RotMatrix( &m_angle, &m_angleMat );
    MulMatrix( &m_angleMat, rottrans );

    // calculate the rottrans matrix for the light
    MulMatrix0( &m_posMat, &m_angleMat, &res );

    // send the resulting light matrix to the GTE
    SetLightMatrix( &res );
}

/*
===================
Send data to the GTE
===================
*/
void Light::Use(void)
{
    // TODO - setbackcolor - variable - is this ambient?
    SetBackColor( 63, 63, 63 );
    SetColorMatrix( &m_colorMat );
}

/*
===================
Setters
===================
*/
void Light::Rotate(   SVECTOR *angle )
{
    m_angle.vx += angle->vx;
    m_angle.vy += angle->vy;
    m_angle.vz += angle->vz;
}
void Light::RotateTo( SVECTOR *angle )
{
    m_angle.vx = angle->vx;
    m_angle.vy = angle->vy;
    m_angle.vz = angle->vz;
}

void Light::Rotate(   int x, int y, int z )
{
    m_angle.vx += x;
    m_angle.vy += y;
    m_angle.vz += z;
}
void Light::RotateTo( int x, int y, int z )
{
    m_angle.vx = x;
    m_angle.vy = y;
    m_angle.vz = z;
}

/*
===================
Getters
===================
*/
SVECTOR *Light::GetAngle(void)
{
    return &m_angle;
}

/*
===================
Default light data
===================
*/
VECTOR  Light::m_DefaultColor = { ONE*3/4, ONE*3/4, ONE*3/4, 0 }; // ONE*3/4
SVECTOR Light::m_DefaultAngle = { 1024, -512, 1024, 0 };
SVECTOR Light::m_DefaultPos   = { ONE, ONE, ONE, 0 };
