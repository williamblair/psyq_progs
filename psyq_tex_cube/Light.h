/*
 * Light.h
 */

#ifndef _LIGHT_H_INCLUDED_
#define _LIGHT_H_INCLUDED_

#include <sys/types.h>	// This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>	// Not necessary but include it anyway
#include <string.h>
#include <stdlib.h>
#include <libetc.h>	// Includes some functions that controls the display
#include <libgte.h>	// GTE header, not really used but libgpu.h depends on it
#include <libgpu.h>	// GPU library header

/*
===============================================================================
Light

    Holds light color and angle matrices, and can send itself to the GTE
    for color calculation
===============================================================================
*/
class Light
{
public:

    /*
    ===================
    Constructor/Deconstructor
    ===================
    */
    Light();
    ~Light();

    /*
    ===================
    Convert internal angle matrix to screen coords
    ===================
    */
    void Translate( MATRIX *rottrans );

    /*
    ===================
    Send light data to the GTE for calculation
    ===================
    */
    void Use(void);

    /*
    ===================
    Getters
    ===================
    */
    SVECTOR *GetAngle(void);

    /*
    ===================
    Setters
    ===================
    */
    void Rotate(   SVECTOR *angle );
    void RotateTo( SVECTOR *angle );

    void Rotate(   int x, int y, int z );
    void RotateTo( int x, int y, int z );

private:
    MATRIX m_angleMat;
    MATRIX m_posMat;
    MATRIX m_colorMat;

    SVECTOR m_angle;
    SVECTOR m_pos;

    static VECTOR  m_DefaultColor;
    static SVECTOR m_DefaultAngle;
    static SVECTOR m_DefaultPos;
};

#endif // _LIGHT_H_INCLUDED_

