/*
 * Pad.cpp
 */

#include "Pad.h"

/*
===================
Constructor/Deconstructor
===================
*/
Pad::Pad(u_int port)
{
	if ( port != 0 && port != 1 ) {
		printf("Invalid port number: %d\n", port);
		printf("Setting port to 1 (technically 0) instead!\n");

		m_port = 0;
	} else {
		m_port = port;
	}

	m_padInfo = 0;
	m_sysPad  = 0;
	m_sysPadT = 0;
}
Pad::~Pad()
{
}

/*
===================
Update pad buffer data
===================
*/
void Pad::Read(void)
{
	/* PSYQ pad read call */
	m_padInfo = PadRead(0);

	/* taken from orion psxlib */
	m_sysPadT = m_padInfo & (m_padInfo ^ m_sysPad);
	m_sysPad  = m_padInfo;
}

/*
===================
Check if a button is held or pressed
===================
*/
bool Pad::IsHeld(u_int button)
{
	return (m_sysPad & _PAD(m_port, button));
}

bool Pad::IsPressed(u_int button)
{
	return (m_sysPadT & _PAD(m_port, button));
}


