#include "Model.h"

/*
===================
Constructor/Deconstructor
===================
*/
Model::Model()
{
    m_vertices  = NULL;
    m_indices   = NULL;
    m_normals   = NULL;
    m_colors    = NULL;
    m_texCoords = NULL;

    m_numVertices = 0;
    m_numFaces    = 0;
    m_numColors   = 0;
    m_numNormals  = 0;

    m_rotation.vx  = 0;
    m_rotation.vy  = 0;
    m_rotation.vz  = 0;
    m_rotation.pad = 0;

    m_translation.vx  = 0;
    m_translation.vy  = 0;
    m_translation.vz  = 0;
    m_translation.pad = 0;

    m_hasTexture = false;
}

Model::~Model()
{
}

/*
===================
Setters
===================
*/
void Model::SetVertices( SVECTOR *vertices, u_int numVertices )
{
    m_vertices = vertices;
    m_numVertices = numVertices;
}
void Model::SetNormals ( SVECTOR *normals,  u_int numVertices )
{
    m_normals = normals;
    m_numVertices = numVertices;
}
void Model::SetFaces   ( INDEX *indices,  u_int numFaces    )
{
    m_indices = indices;
    m_numFaces = numFaces;
}

/*
===================
Transformations
===================
*/
void Model::MoveTo  ( SVECTOR *pos )
{
    m_translation.vx = pos->vx;
    m_translation.vy = pos->vy;
    m_translation.vz = pos->vz;
    m_translation.pad = pos->pad;
}
void Model::MoveTo( long x, long y, long z )
{
    m_translation.vx = x;
    m_translation.vy = y;
    m_translation.vz = z;
    m_translation.pad = 0;
}

void Model::RotateTo( SVECTOR *rot )
{
    m_rotation.vx = rot->vx;
    m_rotation.vy = rot->vx;
    m_rotation.vz = rot->vx;
    m_rotation.pad = rot->pad;
}
void Model::RotateTo( long angX, long angY, long angZ )
{
    m_rotation.vx = angX;
    m_rotation.vy = angY;
    m_rotation.vz = angZ;
    m_rotation.pad = 0;
}

void Model::Move( SVECTOR *pos )
{
    m_translation.vx += pos->vx;
    m_translation.vy += pos->vy;
    m_translation.vz += pos->vz;
}

void Model::Move( long x, long y, long z )
{
    m_translation.vx += x;
    m_translation.vy += y;
    m_translation.vz += z;
}

void Model::Rotate( SVECTOR *rot )
{
    m_rotation.vx += rot->vx;
    m_rotation.vy += rot->vy;
    m_rotation.vz += rot->vz;
}

void Model::Rotate( long angX, long angY, long angZ )
{
    m_rotation.vx += angX;
    m_rotation.vy += angY;
    m_rotation.vz += angZ;
}

/*
===================
Draw the model
===================
*/
void Model::Draw( Light *light )
{
    int i;
    int isomote;
    POLY_F3 *pol3 = NULL; // pointer to current primitive we're calculating
    POLY_FT3 *polt3 = NULL; // current textured primitive, if we have a texture
    MATRIX mtx, lmtx; // local work matrix/local work light matrix

    long p, otz, flg; // outputs from RotAverageNclip3

    long *x0, *x1, *x2; // outputs from GTE transformation

    // default regular color for a texture (gray/no hue change)
    static CVECTOR texColorVec = (CVECTOR){127, 127, 127, 1};
    CVECTOR *cvector_in;    // input color from either pol3 or polt3
    CVECTOR *cvector_out;   // output color calculated by GTE
    void *prim; // points to either pol3 or polt3 primitive

    /* Calculate rotation and translation to the matrix */
    RotMatrix(   &m_rotation, &mtx );
    TransMatrix( &mtx, &m_translation );

    // convert light to screen coordinates for model if given
    if ( light != NULL ) {
        light->Use();
        light->Translate( &mtx );
    }

    /* Save the current matrix register */
    PushMatrix();

    // Set rotation and translation matrix in the GTE
    SetRotMatrix( &mtx );
    SetTransMatrix( &mtx );

    // get the pointer to the next available packet area
    if ( m_hasTexture ) {
        polt3 = (POLY_FT3*)System::GetNextPrimitive();
    }
    else {
        pol3 = (POLY_F3*)System::GetNextPrimitive();
    }

    /* Draw each face */
    for ( i = 0; i < m_numFaces; ++i )
    {
        /* Initialize the primitive */
        if ( m_hasTexture ) {
            int texWidth  = 0;
            int texHeight = 0;

            setPolyFT3(polt3);

            x0 = (long *)&polt3->x0;
            x1 = (long *)&polt3->x1;
            x2 = (long *)&polt3->x2;

            cvector_out = (CVECTOR*)&polt3->r0;
            cvector_in = &texColorVec;

            prim = (void*)polt3;

            // set UV coordinates
            texWidth = m_texture.GetWidth();
            texHeight = m_texture.GetHeight();

            setUV3(
                polt3,
                m_texCoords[m_indices[i].v0].u*texWidth, // U0, V0
                m_texCoords[m_indices[i].v0].v*texHeight,
                m_texCoords[m_indices[i].v1].u*texWidth, // U1, V1
                m_texCoords[m_indices[i].v1].v*texHeight,
                m_texCoords[m_indices[i].v2].u*texWidth, // U2, V2
                m_texCoords[m_indices[i].v2].v*texHeight
            );

            polt3->tpage = m_texture.GetTexturePageID();
            polt3->clut = m_texture.GetClutID();
        }
        else {
            setPolyF3(pol3);

            x0 = (long *)&pol3->x0;
            x1 = (long *)&pol3->x1;
            x2 = (long *)&pol3->x2;

            cvector_out = (CVECTOR*)&pol3->r0;

            prim = (void*)pol3;
        }


        /* Translate from local to screen coordinates
         * otz represents 1/4 value of the average of z value of
         * each vertex */
        isomote = RotAverageNclip3(
            // input coordinates
            &m_vertices[m_indices[i].v0],
            &m_vertices[m_indices[i].v1],
            &m_vertices[m_indices[i].v2],

            // output resulting coordinates
            //(long *)&pol3->x0,
            //(long *)&pol3->x1,
            //(long *)&pol3->x2,
            x0, // either pol3 or polt3 pointers
            x1,
            x2,

            // information outputs
            &p,
            &otz,
            &flg
        );

        /* if result doesn't need to be drawn */
        if (isomote <= 0) {
            //printf("Isomote <=0!\n");
            continue;
        }

        /* Verify resulting z is within OT range */
        if (otz > 0 && otz < OT_LEN) {

            /* Calculate colors */

            // TODO - optimize - this is probs slow
            if ( m_colors != NULL ) {
                /*setRGB0(
                    pol3,
                    m_colors[i].r,
                    m_colors[i].g,
                    m_colors[i].b
                );*/
                // NormalColorCol() overwrite the code field of the prim
                //m_colors[i].cd = pol3->code;
                //m_colors[i].cd = (m_hasTexture) ? polt3->code : pol3->code;
                cvector_in->cd = (m_hasTexture) ? polt3->code : pol3->code;
                NormalColorCol(
                    &m_normals[i],      // input normals
                    //&m_colors[i],       // input colors
                    cvector_in,           // input colors
                    //(CVECTOR*)&pol3->r0 // output calculated color
                    cvector_out // points to color for pol3 or polt3
                );
            } else {
                //setRGB0(pol3, 255, 0, 0);
                setRGB0(((POLY_F3*)prim), 127, 127, 127);
            }

            //System::AddPrimitive(otz, pol3);
            System::AddPrimitive(otz, prim);
        }

        // move the pointer to the next packet area
        //++pol3;
        if (m_hasTexture) {
            ++polt3;
        }
        else {
            ++pol3;
        }
    }

    System::SetNextPrimitive((char*)pol3);

    /* Restore the matrix register */
    PopMatrix();
}

/*
===================
Returns a reference to the texture,
    also sets the internal hasTexture flag to true
===================
*/
Texture *Model::GetTexture(void)
{
    m_hasTexture = true;
    return &m_texture;
}
