#ifndef _TEXTURE_H_INCLUDED_
#define _TEXTURE_H_INCLUDED_

#include <sys/types.h>	// This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>	// Not necessary but include it anyway
#include <string.h>
#include <stdlib.h>
#include <libetc.h>	// Includes some functions that controls the display
#include <libgte.h>	// GTE header, not really used but libgpu.h depends on it
#include <libgpu.h>	// GPU library header

// TODO - this is probably already defined somewhere in PSYQ
enum TEXTURE_BPP {
    TEXTURE_16BIT,
    TEXTURE_8BIT,
    TEXTURE_4BIT,
};

/*
===============================================================================
Texture

    Stores VRAM texture page and clut page information and loads data
    into VRAM
===============================================================================
*/
class Texture
{
public:

    /*
    ===================
    Constructor/Deconstructor
    ===================
    */
    Texture();
    ~Texture();

    /*
    ===================
    Load texture data into VRAM
    ===================
    */
    void Load(
        u_long *texdata,  // pointer to texture data
        u_long *clutdata, // pointer to CLUT data (NULL if none)
        TEXTURE_BPP bpp,  // 0 - 16bit, 1 - 8bit, 2 - 4 bit
        int x,        // where in VRAM to load the texture
        int y,
        int w,        // size of the texture
        int h,
        int clutX,    // where to upload the clut data (if given)
        int clutY
    );

    /*
    ===================
    Getters
    ===================
    */
    u_short GetTexturePageID(void); // GetTPage() is already a function in PSYQ
    u_short GetClutID       (void);

    int GetWidth (void);
    int GetHeight(void);

private:
    u_short m_tpageID; // texture page number returned by LoadTPage
    u_short m_clutID;  // CLUT number returned by LoadClut
    int m_width;
    int m_height;

    bool m_wasLoaded; // true if Load() has been called
};

#endif // _TEXTURE_H_INCLUDED_
