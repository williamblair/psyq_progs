/*
 * Texture.cpp
 */

#include "Texture.h"

/*
===================
Constructor/Deconstructor
===================
*/
Texture::Texture()
{
    m_clutID  = 0;
    m_tpageID = 0;

    m_width  = 0;
    m_height = 0;

    m_wasLoaded = false;
}

Texture::~Texture()
{
}

/*
===================
Load texture data into VRAM
===================
*/
void Texture::Load(
    u_long *texdata,  // pointer to texture data
    u_long *clutdata, // pointer to CLUT data (NULL if none)
    TEXTURE_BPP bpp,  // 0 - 16bit, 1 - 8bit, 2 - 4 bit
    int x,        // where in VRAM to load the texture
    int y,
    int w,        // size of the texture
    int h,
    int clutX,    // where to upload the clut data (if given)
    int clutY
)
{
    if ( clutdata != NULL ) {
        m_clutID = LoadClut(
            clutdata,         // clut pointer
            clutX, clutY      // VRAM coordinates
        );
    }

    m_tpageID = LoadTPage(
        texdata,
        bpp,    // bit depth - 0=4bit, 1=8bit, 2=16bit
        0,      // semitransparency rate (default 0)
        x,      // x - destination from buffer address x
        y,      // y - destination frame buffer address y
        w,      // w - width
        h       // h - height
    );

    m_width  = w;
    m_height = h;

    m_wasLoaded = true;
}

/*
===================
Getters
===================
*/
u_short Texture::GetTexturePageID(void)
{
    return m_tpageID;
}
u_short Texture::GetClutID(void)
{
    return m_clutID;
}
int Texture::GetWidth (void)
{
    return m_width;
}
int Texture::GetHeight(void)
{
    return m_height;
}
