/*
 * Pad.h
 */

#ifndef PSX_PAD_INCLUDED
#define PSX_PAD_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <libgte.h>
#include <libgpu.h>
#include <libgs.h>
#include <libetc.h>
#include <libpad.h>

/* modified from Orion's psx library */
#define PadUp           PADLup
#define PadDown	        PADLdown
#define PadLeft	        PADLleft
#define PadRight		PADLright
#define PadTriangle     PADRup
#define PadCross		PADRdown
#define PadSquare		PADRleft
#define PadCircle		PADRright
#define PadL1			PADL1
#define PadL2			PADL2
#define PadR1			PADR1
#define PadR2			PADR2
#define PadStart		PADstart
#define PadSelect		PADselect

/*
===============================================================================
Pad

    Reads and holds pad buffer data for a controller, either port 1 or 2
===============================================================================
*/
class Pad
{
public:

    /*
    ===================
    Constructor/Deconstructor
    ===================
    */
    Pad(u_int port);
    ~Pad();

    /*
    ===================
    Update pad buffer data
    ===================
    */
    void Read(void);

    /*
    ===================
    Check if a button is held or pressed
    ===================
    */
    bool IsHeld(    u_int button ); // returns true if sent button is held down
    bool IsPressed( u_int button ); // returns true if sent button was clicked

private:

    int m_port; // is it controller 1 or 2?

    int m_padInfo; // contains read in pad information

    /* these 2 taken from Orion's psx lib
     * used for pad info manipulation */
    int m_sysPad;
    int m_sysPadT;
};

#endif // _PAD_H_INCLUDED_
