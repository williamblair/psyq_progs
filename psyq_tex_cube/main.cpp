
#include <stdio.h>

#include "Light.h"
#include "System.h"
#include "Model.h"
#include "Pad.h"
#include "Cube.h"

#include "psxlogo_arr.h"

// for(ever) ;)
#define ever ;;

int main(void)
{
    Cube cube;
    Cube cube2;
    Light light;
    SVECTOR *lightAngle;

    Pad pad1(0);

    // reset gpu, sys, etc.
    System::Init();

    cube.MoveTo(0, 0, 500);
    cube2.MoveTo(-200, 0, 400);

    /*u_short clut =
    LoadClut(
        (u_long*)psxlogo_clut, // clut pointer
        320, 256      // long x, long y
    );

    u_short tpage =
    LoadTPage(
        (u_long*)psxlogo_arr,
        1,   // bit depth - 0=4bit, 1=8bit, 2=16bit
        0,   // semitransparency rate (default 0)
        320, // x - destination from buffer address x
        0,   // y - destination frame buffer address y
        psxlogo_width,// w - width
        psxlogo_height// h - height
    );*/

    /*POLY_FT4 spr;
    setPolyFT4(&spr);
    setShadeTex(&spr, 1); // shaded texture disable
    spr.tpage = tpage;
    spr.clut = clut;
    setUVWH(&spr, 0,0, 64,64);
    setXYWH(&spr, 10, 10, 64, 64);
    POLY_FT3 spr1;
    setPolyFT3(&spr1);
    setShadeTex(&spr1, 1);
    spr1.tpage = tpage;
    spr1.clut = clut;
    setUV3(
        &spr1,
        0, psxlogo_height,  // UV point 1
        psxlogo_width, 0,   // UV point 2
        0, 0                // UV point 3
    );
    setXY3(
        &spr1,
        10, 10+psxlogo_height, // XY point 1
        10+psxlogo_width, 10,  // XY point 2
        10, 10                 // XY point 3
    );*/
    cube.GetTexture()->Load(
        (u_long*)psxlogo_arr,
        (u_long*)psxlogo_clut,
        TEXTURE_8BIT,
        320, 0,         // x,y in VRAM
        64, 64,         // width, height
        320, 256        // clut x,y
    );

    for(ever)
    {
        pad1.Read();

        // rotate cube
        if ( pad1.IsHeld(PadUp) ) {
            cube.Rotate(5, 0, 0);
        } else if ( pad1.IsHeld(PadDown) ) {
            cube.Rotate(-5, 0, 0);
        }
        else if ( pad1.IsHeld(PadLeft) ) {
            cube.Rotate(0, -5, 0);
        }
        else if (pad1.IsHeld(PadRight)) {
            cube.Rotate(0, 5, 0);
        }

        // rotate light
        if ( pad1.IsHeld(PadTriangle) ) {
            light.Rotate(5, 0, 0);
        } else if ( pad1.IsHeld(PadCross) ) {
            light.Rotate(-5, 0, 0);
        }
        else if ( pad1.IsHeld(PadSquare) ) {
            light.Rotate(0, -5, 0);
        }
        else if (pad1.IsHeld(PadCircle)) {
            light.Rotate(0, 5, 0);
        }

        cube2.Rotate(8, 0, 15);

        cube.Draw(&light);
        cube2.Draw(&light);
        //cube.Draw((Light*)NULL);
        //cube.Draw(&lgtang, &rotlgt, &lgtmat, &light);
        //cube2.Draw(&lgtang, &rotlgt, &lgtmat, &light);
        //cube2.Draw((Light*)NULL);

        // alternatively - draw prim
        //System::AddPrimitive(OT_LEN-4, (void*)&spr1);

        //System::DrawText(S_WIDTH>>1, S_HEIGHT>>1, "Hello World");
        lightAngle = light.GetAngle();
        FntPrint("Light Angle: %d, %d, %d", lightAngle->vx, lightAngle->vy, lightAngle->vz);

        System::Display();
    }

    return 0;
}


