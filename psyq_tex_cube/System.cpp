#include "System.h"

namespace System
{

typedef struct DB {

    DISPENV disp;
    DRAWENV draw;

    unsigned int ot[OT_LEN];     // order table buffer
    char         p [PACKET_LEN]; // packet list
} DB;

DB db[2]; // 2 for double buffer
int db_active = 0; // current active db (0 or 1)
char *db_nextpri; // pointer to entry in p where the next packet should go

/*
===================
ResetGraph, initialize display and draw envs
===================
*/
void Init()
{

    // Reset graphics
    ResetGraph(0);

    // First buffer
    SetDefDispEnv( &db[0].disp, 0, 0, S_WIDTH, S_HEIGHT );
    SetDefDrawEnv( &db[0].draw, 0, S_HEIGHT, S_WIDTH, S_HEIGHT );

    // Second buffer
    SetDefDispEnv( &db[1].disp, 0, S_HEIGHT, S_WIDTH, S_HEIGHT );
    SetDefDrawEnv( &db[1].draw, 0, 0, S_WIDTH, S_HEIGHT );

    db[0].draw.isbg = 1;               // Enable clear
    setRGB0( &db[0].draw, 63, 0, 127 );  // Set clear color
    // db[0].draw.dtd = 1; // enable dither processing

    db[1].draw.isbg = 1;
    setRGB0( &db[1].draw, 63, 0, 127 );
    // db[1].draw.dtd = 1; // enable dither processing

    // apply the first drawing env
    PutDrawEnv( &db[0].draw );

    // Load debug bios font
    FntLoad(960, 256); // load basic font pattern
    // screen X,Y | max text length X,Y | autmatic background clear 0,1 | max characters (eg: 50).
	SetDumpFnt(FntOpen(5, 20, S_WIDTH, S_HEIGHT, 0, 512));

    // clear order tables
    ClearOTagR( (u_long *)db[0].ot, OT_LEN );
    ClearOTagR( (u_long *)db[1].ot, OT_LEN );

    // set primitive pointer address
    db_nextpri = db[0].p;

    // init the gte
    InitGeom();

    // set GTE offset (recommended method of centering)
    // set to center of screen (width/2, height/2);
    //gte_SetGeomOffset( S_WIDTH >> 1 , S_HEIGHT >> 1 );
    SetGeomOffset( S_WIDTH >> 1, S_HEIGHT >> 1 );

    // set screen depth (FOV control, width/2 works best)
    //gte_SetGeomScreen( S_WIDTH >> 1 );
    SetGeomScreen(S_WIDTH >> 1);

    // init the controller system
    PadInit(0);
}

/*
===================
Draw the next display buffer
===================
*/
void Display(void)
{
    FntFlush(-1);

    DrawSync(0);
    VSync(0);               // Wait for vertical retrace

    db_active = !db_active;               // Swap buffers on every pass (alternates between 1 and 0)
    db_nextpri = db[db_active].p;         // reset the primitive ot pointer

    ClearOTagR( (u_long*)db[db_active].ot, OT_LEN );

    PutDispEnv(&db[db_active].disp);  // Apply the DISPENV/DRAWENVs
    PutDrawEnv(&db[db_active].draw);

    SetDispMask(1);         // Enable the display

    DrawOTag( (u_long*)db[1-db_active].ot+(OT_LEN-1) ); // start drawing the OT of the LAST buffer
}

void AddPrimitive(int offset, void *prim)
{
    addPrim( db[db_active].ot+(offset), prim );
}

void DrawText(int x, int y, const char *message)
{
    // Might need to edit OT for offset...
    //db_nextpri = FntSort( (u_long*)&db[db_active].ot[OT_LEN-1], db_nextpri, x, y, message );
    FntPrint(message);
}

/*
===================
Get the current active OT buffer
===================
*/
u_int *GetCurrentOT(void)
{
    return db[db_active].ot;
}

char *GetNextPrimitive(void)
{
    return db_nextpri;
}

void SetNextPrimitive(char *prim)
{
    db_nextpri = prim;
}

} // end namespace System

