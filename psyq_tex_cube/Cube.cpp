/*
 * Cube.cpp
 */

#include "Cube.h"

/*
===================
Constructor/Deconstructor
===================
*/
Cube::Cube()
{
    m_numFaces    = NUM_CUBE_FACES;
    m_numVertices = NUM_CUBE_VERTICES;
    m_numNormals  = NUM_CUBE_FACES;
    m_numColors   = NUM_CUBE_FACES;

    m_vertices  = m_DefaultVertices;
    m_indices   = m_DefaultFaces;
    m_colors    = m_DefaultColors;
    m_normals   = m_DefaultNormals;
    m_texCoords = m_DefaultTexCoords;
}
Cube::~Cube()
{
}

/*
===================
Default Cube data
===================
*/
SVECTOR Cube::m_DefaultVertices[NUM_CUBE_VERTICES] = {
    { -100, -100, -100, 0 }, // front bottom left   0
    {  100, -100, -100, 0 }, // front bottom right  1
    { -100,  100, -100, 0 }, // front top left      2
    {  100,  100, -100, 0 }, // front top right     3
    {  100, -100,  100, 0 }, // back bottom right   4
    { -100, -100,  100, 0 }, // back bottom left    5
    {  100,  100,  100, 0 }, // back top right      6
    { -100,  100,  100, 0 }  // back top left       7
};

// These need to be scaled by the actual width and height of the texture
TEXCOORD Cube::m_DefaultTexCoords[NUM_CUBE_VERTICES] = {
    {0, 1}, // front bottom left
    {1, 1}, // front bottom right
    {0, 0}, // front top left
    {1, 0}, // front top right

    {0, 1}, // front bottom left
    {1, 1}, // front bottom right
    {0, 0}, // front top left
    {1, 0} // front top right

    /*{1, 1}, // back bottom right
    {0, 1}, // back bottom left
    {1, 0}, // back top right
    {0, 0}  // back top left*/
};

SVECTOR Cube::m_DefaultNormals[NUM_CUBE_FACES] = {
    { 0, 0,  -ONE,   0 }, // front
    { 0, 0,  -ONE,   0 },

    { 0, 0, ONE,   0 }, // back
    { 0, 0, ONE,   0 },

    { 0,   -ONE, 0, 0 }, // bottom
    { 0,   -ONE, 0, 0 },

    { 0,   ONE,  0, 0 }, // top
    { 0,   ONE,  0, 0 },

    { -ONE, 0, 0, 0 }, // left
    { -ONE, 0, 0, 0 },

    { ONE, 0, 0, 0 }, // right
    { ONE, 0, 0, 0 }
};
CVECTOR Cube::m_DefaultColors[NUM_CUBE_FACES] = {
    { 255, 0,   0,   0 },
    { 255, 0,   0,   0 },

    { 0,   255, 0,   0 },
    { 0,   255, 0,   0 },

    { 0,   0,   255, 0 },
    { 0,   0,   255, 0 },

    { 0,   0,   255, 0 },
    { 0,   0,   255, 0 },

    { 255, 0,   255, 0 },
    { 255, 0,   255, 0 },

    { 0,   255, 255, 0 },
    { 0,   255, 255, 0 }
};
INDEX   Cube::m_DefaultFaces[NUM_CUBE_FACES] = {
    { 0, 1, 2, 0 }, // front
    { 1, 3, 2, 0 },

    { 4, 5, 6, 0 }, // back
    { 5, 7, 6, 0 },

    { 5, 4, 0, 0 }, // bottom
    { 4, 1, 0, 0 },

    { 6, 7, 3, 0 }, // top
    { 7, 2, 3, 0 },

    { 0, 2, 5, 0 }, // left
    { 2, 7, 5, 0 },

    { 3, 1, 6, 0 }, // right
    { 1, 4, 6, 0 }
};
