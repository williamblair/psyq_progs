/*
 * Model.h
 */

#ifndef _MODEL_H_INCLUDED_
#define _MODEL_H_INCLUDED_

#include <sys/types.h>
#include <stddef.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <stdio.h>

#include "System.h"

/*
 * Vertex faces indices
 */
typedef struct INDEX {
    unsigned int v0, v1, v2, v3;
} INDEX;

/*
===============================================================================
Model

    Encapsulates data and drawing/transformation functions for a 3d model
===============================================================================
*/
class Model
{
public:

    /*
    ===================
    Constructor/Deconstructor
    ===================
    */
    Model();
    ~Model();

    /*
    ===================
    Setters
    ===================
    */
    void SetVertices( SVECTOR *vertices, u_int numVertices );
    void SetNormals ( SVECTOR *normals,  u_int numVertices );
    void SetFaces   ( INDEX *indices,  u_int numFaces    );

    /*
    ===================
    Transformations
    ===================
    */
    void MoveTo   ( SVECTOR *pos ); // set explicitly
    void RotateTo ( SVECTOR *rot );
    void MoveTo   ( long x,    long y,    long z );
    void RotateTo ( long angX, long angY, long angZ );

    void Move( SVECTOR *pos ); // add to the current value
    void Move( long x, long y, long z );

    void Rotate( SVECTOR *rot );
    void Rotate( long angX, long angY, long angZ );

    /*
    ===================
    Draw the model
    ===================
    */
    void Draw( MATRIX *lightMatrix );

private:

    /*
    ===================
    3D Data pointers
    (data it points to MUST exist during the model's lifetime)
    ===================
    */
    SVECTOR *m_vertices;
    SVECTOR *m_normals;
    INDEX   *m_indices;

    u_int m_numVertices; // num vertices should equal num normals
    u_int m_numNormals;
    u_int m_numFaces; // size of m_indices

    /*
    ===================
    Transformation variables
    ===================
    */
    SVECTOR m_rotation;
    VECTOR  m_translation;
};

#endif // _MODEL_H_INCLUDED_
