
#include <stdio.h>

#include "Light.h"
#include "System.h"
#include "Model.h"
#include "Pad.h"

#include "bunnyArr.h"

// for(ever) ;)
#define ever ;;

SVECTOR light_color = {ONE, 0, 0, 0}; // red color
SVECTOR light_dir   = {-2048, -2048, -2048, 0}; // pointing down i think

int main(void)
{
    Light light;
    Model bunny;
    Pad pad1(0);

    // reset gpu, sys, etc.
    System::Init();

    Light_Init( &light );
    Light_SetColor( &light, &light_color, 0 );
    Light_SetDirection( &light, &light_dir, 0 );
    Light_Use( &light );

    bunny.SetVertices( bunnyVertices, numBunnyVertices );
    bunny.SetFaces   ( bunnyIndices,  numBunnyIndices  );
    bunny.SetNormals ( bunnyNormals,  numBunnyVertices );

    bunny.MoveTo  ( 0,   0,  1500 );
    bunny.RotateTo( 100, 0, -100 );

    for(ever)
    {
        pad1.Read();

        /*
         * Rotation
         */
        if ( pad1.IsHeld(PadRight) ) {
            bunny.Rotate( 0, 5, 0 );
        }
        else if (pad1.IsHeld(PadLeft) ) {
            bunny.Rotate( 0, -5, 0 );
        }
        else if (pad1.IsHeld(PadUp) ) {
            bunny.Rotate( 5, 0, 0 );
        }
        else if (pad1.IsHeld(PadDown) ) {
            bunny.Rotate( -5, 0, 0 );
        }

        /*
         * Position
         */
        if ( pad1.IsHeld(PadCircle) ) {
            bunny.Move( 5, 0, 0 );
        }
        else if ( pad1.IsHeld(PadSquare) ) {
            bunny.Move( -5, 0, 0 );
        }
        else if ( pad1.IsHeld(PadTriangle) ) {
            bunny.Move( 0, 0, 5 );
        }
        else if ( pad1.IsHeld(PadCross) ) {
            bunny.Move( 0, 0, -5 );
        }

        bunny.Draw( &light.directionMatrix );

        System::DrawText(S_WIDTH>>1, S_HEIGHT>>1, "Hello World");

        System::Display();
    }

    return 0;
}


