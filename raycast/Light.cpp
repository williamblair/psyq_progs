/*
 * Light.cpp
 */

#include "Light.h"

void Light_Init( Light *light )
{
    memset( &light->colorMatrix,     0, sizeof(light->colorMatrix)     );
    memset( &light->directionMatrix, 0, sizeof(light->directionMatrix) );
}


/*
 * sets the respective column of the matrix to the given color
 * colNum should be 0, 1, or 2
 */
void Light_SetColor(Light *light, SVECTOR *color, int colNum)
{
    // Matrices are in [row][col] format
    light->colorMatrix.m[0][colNum] = color->vx;
    light->colorMatrix.m[1][colNum] = color->vy;
    light->colorMatrix.m[2][colNum] = color->vz;
}

/*
 * sets each respective row of the direction matrix
 * rowNum should be 0, 1, or 2
 */
void Light_SetDirection(Light *light, SVECTOR *dir, int rowNum)
{
    light->directionMatrix.m[rowNum][0] = dir->vx;
    light->directionMatrix.m[rowNum][1] = dir->vy;
    light->directionMatrix.m[rowNum][2] = dir->vz;
}

/*
 * Send the light to the GTE for usage
 */
void Light_Use( Light *light )
{
    // TODO - setbackcolor - ambient?
    //gte_SetBackColor(   63, 63, 63          );
    SetBackColor( 63, 63, 63 );
    //gte_SetColorMatrix( &light->colorMatrix );
    SetColorMatrix( &light->colorMatrix );
}


