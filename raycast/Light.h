/*
 * Light.h
 */

#ifndef _LIGHT_H_INCLUDED_
#define _LIGHT_H_INCLUDED_

#include <sys/types.h>	// This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>	// Not necessary but include it anyway
//#include <string.h>
#include <string.h>
#include <stdlib.h>
#include <libetc.h>	// Includes some functions that controls the display
#include <libgte.h>	// GTE header, not really used but libgpu.h depends on it
#include <libgpu.h>	// GPU library header

typedef struct Light
{
    MATRIX colorMatrix;
    MATRIX directionMatrix;

} Light;

/*
 * Sets all zeros
 */
void Light_Init( Light *light );

/*
 * sets the respective column of the matrix to the given color
 * colNum should be 0, 1, or 2
 */
void Light_SetColor(Light *light, SVECTOR *color, int colNum);

/*
 * sets each respective row of the direction matrix
 * rowNum should be 0, 1, or 2
 */
void Light_SetDirection(Light *light, SVECTOR *dir, int rowNum);

/*
 * Send the light to the GTE for usage
 */
void Light_Use( Light *light );

#endif // _LIGHT_H_INCLUDED_

