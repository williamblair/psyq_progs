#include "Model.h"

/*
===================
Constructor/Deconstructor
===================
*/
Model::Model()
{
    m_vertices = NULL;
    m_indices  = NULL;
    m_normals  = NULL;

    m_numVertices = 0;
    m_numNormals  = 0;
    m_numFaces    = 0;

    m_rotation.vx  = 0;
    m_rotation.vy  = 0;
    m_rotation.vz  = 0;
    m_rotation.pad = 0;

    m_translation.vx  = 0;
    m_translation.vy  = 0;
    m_translation.vz  = 0;
    m_translation.pad = 0;
}

Model::~Model()
{
}

/*
===================
Setters
===================
*/
void Model::SetVertices( SVECTOR *vertices, u_int numVertices )
{
    m_vertices = vertices;
    m_numVertices = numVertices;
}
void Model::SetNormals ( SVECTOR *normals,  u_int numVertices )
{
    m_normals = normals;
    m_numVertices = numVertices;
}
void Model::SetFaces   ( INDEX *indices,  u_int numFaces    )
{
    m_indices = indices;
    m_numFaces = numFaces;
}

/*
===================
Transformations
===================
*/
void Model::MoveTo  ( SVECTOR *pos )
{
    m_translation.vx = pos->vx;
    m_translation.vy = pos->vy;
    m_translation.vz = pos->vz;
    m_translation.pad = pos->pad;
}
void Model::MoveTo( long x, long y, long z )
{
    m_translation.vx = x;
    m_translation.vy = y;
    m_translation.vz = z;
    m_translation.pad = 0;
}

void Model::RotateTo( SVECTOR *rot )
{
    m_rotation.vx = rot->vx;
    m_rotation.vy = rot->vx;
    m_rotation.vz = rot->vx;
    m_rotation.pad = rot->pad;
}
void Model::RotateTo( long angX, long angY, long angZ )
{
    m_rotation.vx = angX;
    m_rotation.vy = angY;
    m_rotation.vz = angZ;
    m_rotation.pad = 0;
}

void Model::Move( SVECTOR *pos )
{
    m_translation.vx += pos->vx;
    m_translation.vy += pos->vy;
    m_translation.vz += pos->vz;
}

void Model::Move( long x, long y, long z )
{
    m_translation.vx += x;
    m_translation.vy += y;
    m_translation.vz += z;
}

void Model::Rotate( SVECTOR *rot )
{
    m_rotation.vx += rot->vx;
    m_rotation.vy += rot->vy;
    m_rotation.vz += rot->vz;
}

void Model::Rotate( long angX, long angY, long angZ )
{
    m_rotation.vx += angX;
    m_rotation.vy += angY;
    m_rotation.vz += angZ;
}

/*
===================
Draw the model
===================
*/
void Model::Draw( MATRIX *lightMatrix )
{
    int p; // clip/z depth test value (for depth calculation,e.g. should this poly be drawn?)
    POLY_F3 *pol3 = NULL; // pointer to current primitive we're calculating
    MATRIX mtx, lmtx; // local work matrix/local work light matrix

    long interp, otz, flag; // outputs from RotAverageNclip3

    /* Set rotation and translation to the matrix */
    RotMatrix(   &m_rotation, &mtx );
    TransMatrix( &mtx, &m_translation );

    // only calculate light stuff if we have normals to work with
    if (m_normals != NULL ) {

        // Set light matrix to model coordinates
        MulMatrix0( lightMatrix, &mtx, &lmtx );

        // Use our translated light matrix
        SetLightMatrix( &lmtx );

    }

    // Set rotation and translation matrix in the GTE
    //gte_SetRotMatrix(   &mtx );
    //gte_SetTransMatrix( &mtx );
    SetRotMatrix( &mtx );
    SetTransMatrix( &mtx );

    // get a pointer of the next primitive in the OT (I think) from the system
    //POLY_F4 *pol4 = (POLY_F4*)Sytem::db_nextpri;
    //POLY_F4 *pol4 = (POLY_F4*)System::GetNextPrimitive();
    //POLY_F3 *pol3 = (POLY_F3*)System::GetNextPrimitive();
    pol3 = (POLY_F3*)System::GetNextPrimitive();


    // draw each cube face
    for (int i=0; i < m_numFaces; ++i)
    {
        //printf("I = %d\n", i);
        // load first three vertices of a quad to the GTE
#if 0
        gte_ldv3(
            &m_vertices[m_indices[i].v0],
            &m_vertices[m_indices[i].v1],
            &m_vertices[m_indices[i].v2]
        );
        //printf("After load v3\n");

        // rotation, translation, and perspective triple
        gte_rtpt();

        // compute normal clip for backface culling
        gte_nclip();

        //printf("After nclip\n");

        // get result from normal clip computation
        //int p;
        gte_stopz( &p );
#endif
        // initialize a quad primitive
        setPolyF3( (POLY_F3*)pol3 );

        p = RotAverageNclip3(
            &m_vertices[m_indices[i].v0], // pointer to vectors (input)
            &m_vertices[m_indices[i].v1],
            &m_vertices[m_indices[i].v2],
            (long*)&pol3->x0, // pointers to output vertices
            (long*)&pol3->x1,
            (long*)&pol3->x2,
            (long*)&interp, &otz, &flag // output info?
        );

        // skip this tri if backfaced
        if ( p < 0 ) {
            continue;
        }

        // calculate average z for depth sorting
        //gte_avsz4();
        //gte_stotz( &p );

        //printf("After stotz\n");

        // skip if clipping off]
        // right shift for scaling depth precision
        //if ((p>>2) > OT_LEN) {
        if (otz > OT_LEN ) {
            continue;
        }


        //SetLineF3( pol3 );

        // fill prim with projected vertices
        //gte_stsxy0( &pol3->x0 );
        //gte_stsxy1( &pol3->x1 );
        //gte_stsxy2( &pol3->x2 );


        //printf("After stsxy\n");
        // load primitve color even though gte_ncs doesnt use it
        // this is so the GTE will output a color result with the
        // correct primitive code

        // dummy - set color for now until normals are computed
        //pol3->r0 = 255; pol3->g0 = 0; pol3->b0 = 0;

        // if we have normals calculate their lighting
        if (m_normals != NULL) {
            /*gte_ldrgb( &pol3->r0 );

            // load the face normal
            gte_ldv0( &m_normals[m_indices[i].v0] );
            //gte_ldv0( &bunnyNormals[i] );

            // normal color single
            gte_ncs();

            // store the result back in the primitve
            gte_strgb( &pol3->r0 );*/
            pol3->r0 = 255; pol3->g0 = 0; pol3->b0 = 0;
        }
        // otherwise set a color
        else {
            // TODO - have material color or something
            pol3->r0 = 255; pol3->g0 = 0; pol3->b0 = 0;
        }
        // sort primitive in the order table
        //addPrim( db->ot+(p>>2), pol4 );
        //printf("Before add prim\n");
        //System::AddPrimitive( p>>2, (void*)pol3 );
        System::AddPrimitive( otz, (void*)pol3 );
        //printf("After add prim\n");

        // advance pointer to make another primitive
        ++pol3;
    }

    // Update the next primitve
    // IMPORTANT if you're drawing more after this
    System::SetNextPrimitive( (char*)pol3 );
}

