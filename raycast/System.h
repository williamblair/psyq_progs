/*
 * System.h
 */

#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED

#include <sys/types.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <stdio.h>

// order table and packet lengths
#define OT_LEN     (8192*2)
#define PACKET_LEN (8192*8)

// screen size
#define S_WIDTH  320
#define S_HEIGHT 240

/*
===============================================================================
System

    PSX initialization and display/draw environments wrapper
===============================================================================
*/
namespace System
{

/*
===================
ResetGraph, initialize display and draw envs
===================
*/
void Init(); // TODO - add bg color

/*
===================
VSyncs, sets and swaps display buffers
===================
*/
void Display();

/*
===================
Add a primitive to be drawn to the ot
===================
*/
void AddPrimitive(int offset, void *prim);

/*
===================
Render message at given location using bios font
(assumes FntLoad() has already been called)
===================
*/
void DrawText(int x, int y, const char *message);

/*
===================
Get and Set next primitive pointer for the order table
===================
*/
char *GetNextPrimitive(void);
void SetNextPrimitive( char *prim );

} // end namespace system

#endif // SYSTEM_H_INCLUDED
