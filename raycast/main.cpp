
#include <stdio.h>

#include "Light.h"
#include "System.h"
#include "Model.h"
#include "Pad.h"

#include "worldMap.h"

// for(ever) ;)
#define ever ;;


// 5 arbitrarily chosen as depth for add primitive - TODO - depth
#define DRAW_LINE(line,x0,y0,x1,y1,r,g,b);   \
    setLineF2(line);                         \
    setXY2(line, x0, y0, x1, y1);            \
    setRGB0(line, r, g, b);                  \
    System::AddPrimitive(5, (void*)line);    \
    ++line;


int main(void)
{
    int x;

    float posX = 22, posY = 12; // x and y start pos
    float dirX = -1, dirY = 0;  // initial direction vector
    float planeX = 0, planeY = 0.66; // 2d raycaster version of camera plane

    float time = 0; // time of current frame
    float oldTime = 0; // time of previous frame

    float moveSpeed = 5.0;
    float rotSpeed = 3.0;

    Pad pad1(0);

    // reset gpu, sys, etc.
    System::Init();

    for(ever)
    {
        // test line draw
        char *nextpri = System::GetNextPrimitive();
        LINE_F2 *line = (LINE_F2*)nextpri;

        System::DrawText(S_WIDTH>>1, S_HEIGHT>>1, "Hello World");
        pad1.Read();

        for (x=0; x<S_WIDTH; ++x)
        {

        int lineHeight;
        int drawStart, drawEnd;
        u_char r,g,b;

        float cameraX = 2 * x / (float)S_WIDTH - 1; // x coordinate in camera space
        float rayDirX = dirX + planeX * cameraX;
        float rayDirY = dirY + planeY * cameraX;

        int mapX = (int)posX;
        int mapY = (int)posY;

        // length of ray from current position to next x or y side
        float sideDistX, sideDistY;

        // length of ray from one x or y side to the next x or y side
        float invDirX = 1.0 / rayDirX;
        float invDirY = 1.0 / rayDirY;
        float deltaDistX = (invDirX < 0) ? -invDirX : invDirX;//abs(1 / rayDirX);
        float deltaDistY = (invDirY < 0) ? -invDirY : invDirY;//abs(1 / rayDirY);
        float perpWallDist;

        // what direction to step in x or y dir (either +1 or -1)
        int stepX, stepY;

        int hit = 0; // true once a wall is hit
        int side;    // was a NS or EW wall hit?

        // calculate step and initial sideDist
        if (rayDirX < 0) {
            stepX = -1;
            sideDistX = (posX - mapX) * deltaDistX;
        } else {
            stepX = 1;
            sideDistX = (mapX + 1.0 - posX) * deltaDistX;
        }

        if (rayDirY < 0) {
            stepY = -1;
            sideDistX = (posY - mapY) * deltaDistY;
        } else {
            stepY = 1;
            sideDistX = (mapY + 1.0 - posY) * deltaDistY;
        }

        // perform DDA
        do
        {
            if (sideDistX < sideDistY) {
                sideDistX += deltaDistX;
                mapX += stepX;
                side = 0;
            } else {
                sideDistY += deltaDistY;
                mapY += stepY;
                side = 1;
            }

            // check if we hit a wall
            if (worldMap[mapX][mapY] > 0) {
                hit = 1;
            }

        } while (hit == 0);

        // calculate distance projected on camera direction
        // (perpindicular - euclidean gives fish eye effect)
        if (side == 0) perpWallDist = (mapX - posX + (1 - stepX) / 2) / rayDirX;
        else           perpWallDist = (mapY - posY + (1 - stepY) / 2) / rayDirY;

        // calculate line height
        lineHeight = (int)(S_HEIGHT / perpWallDist);

        // calculate lowest and highest pixel to fill in current stripe
        drawStart = -lineHeight / 2 + S_HEIGHT / 2;
        if (drawStart < 0) drawStart = 0;
        drawEnd = lineHeight / 2 + S_HEIGHT / 2;
        if (drawEnd >= S_HEIGHT) drawEnd = S_HEIGHT - 1;

        // choose wall color
        r = 0;
        g = 0;
        b = 0;
        switch (worldMap[mapX][mapY])
        {
        case 1: r = 255; break; // red
        case 2: g = 255; break; // green
        case 3: b = 255; break; // blue
        case 4: r = 255; g = 255; b = 255; break;// white
        default: r = 255; g = 255; break; // yellow
        }

        // give x and y sides different brightness
        if (side == 1) {
            r >>= 1;
            g >>= 1; // divide by 2
            b >>= 1;
        }

        DRAW_LINE(line, x, drawStart, x, drawEnd, r, g, b);

        //for (x=0; x<S_WIDTH; ++x)
        //{
            //DRAW_LINE(line, x, 100, x, 200, 255, 0, 0);
        //}
        }
        System::SetNextPrimitive((char*)line);
        System::Display();

        // check controller
        if ( pad1.IsHeld(PadLeft) ) {
            float oldPlaneX = planeX;
            float oldDirX = dirX;

            dirX = dirX * cos(rotSpeed) - dirY * sin(rotSpeed);
            dirY = oldDirX * sin(rotSpeed) + dirY * cos(rotSpeed);

            planeX = planeX * cos(rotSpeed) - planeY * sin(rotSpeed);
            planeY = oldPlaneX * sin(rotSpeed) + planeY * cos(rotSpeed);
        }
        else if ( pad1.IsHeld(PadRight) ) {
            float oldPlaneX = planeX;
            float oldDirX = dirX;

            dirX = dirX * cos(-rotSpeed) - dirY * sin(-rotSpeed);
            dirY = oldDirX * sin(-rotSpeed) + dirY * cos(-rotSpeed);

            planeX = planeX * cos(-rotSpeed) - planeY * sin(-rotSpeed);
            planeY = oldPlaneX * sin(-rotSpeed) + planeY * cos(-rotSpeed);

        }
        else if ( pad1.IsHeld(PadUp) ) {
            printf("Up!\n");
        }
        else if ( pad1.IsHeld(PadDown) ) {
            printf("Down!\n");
        }
    }

    return 0;
}


